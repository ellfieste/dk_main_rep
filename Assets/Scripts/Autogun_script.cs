using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Autogun_script : MonoBehaviour
{
    [SerializeField] Transform _rotatingPart;
    [SerializeField] Transform _player;
    [SerializeField] Transform _firePoint;
    [SerializeField] GameObject _bulletPref;
    public float bulletForce = 20f;
    private float firetic = 80f;
    Quaternion rot = new Quaternion(0, 180, 0, 0);

    //�������
    //�����������
    private byte bulletsWasSpawn = 0; //��������� ���� �������
    public byte bulletsForOverload = 4; //��������� ��� ��������� � ���������� �������
    [SerializeField]private float overloadTime = 0f; //����� ���������
    public float overloadTimeValue = 0f; //����� ��������� ��������
    //��������� �����
    //����������
    public void RotatingPart()
    {
        _rotatingPart.transform.LookAt(_player);
        _rotatingPart.transform.rotation = _rotatingPart.transform.rotation * rot;

    }
    public void Fire()
    {
        if (firetic >= 80f)
        {
            GameObject bullet = Instantiate(_bulletPref, _firePoint.position, _firePoint.rotation);
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            rb.AddForce(_firePoint.forward * bulletForce, ForceMode.Impulse);
            bulletsWasSpawn++;
            firetic = 0f;
        }

    }
    public void Overload()
    {
        overloadTime++;
        if (overloadTime == overloadTimeValue)
        {
            bulletsWasSpawn = 0;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (bulletsWasSpawn < bulletsForOverload)
            {
                RotatingPart();
                Fire();
                firetic++;
                overloadTime = 0f;
            }
            else Overload();
        }
    }
}
