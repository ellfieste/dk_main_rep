using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCont : MonoBehaviour
{
    [SerializeField]Transform playerTransform;
    public Vector3 offset;
    public float moveSpeed = 5f;
    private void FixedUpdate()
    {
        Vector3 newCamPosition = new Vector3(playerTransform.position.x + offset.x, offset.y, playerTransform.position.z+offset.z);
        transform.position = Vector3.Lerp(transform.position, newCamPosition, moveSpeed * Time.deltaTime);
        //transform.position = new Vector3 (transform.position.x, transform.position.y, (playerTransform.transform.position.z+high)*Time.deltaTime);
    }
}
