using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private FixedJoystick _fixedJoystick;
    [SerializeField] private Animator _animator;
    [SerializeField] private float _moveSpeed;
    public GameObject _uiAnimator;
    public byte _hp = 3;
    private void FixedUpdate()
    {
        _rigidbody.velocity = new Vector3(_fixedJoystick.Horizontal * _moveSpeed, _rigidbody.velocity.y, _fixedJoystick.Vertical * _moveSpeed);
        Animations();
        Die();
    }
    private void Animations()
    {
        if (_fixedJoystick.Horizontal != 0f || _fixedJoystick.Vertical != 0f)
        {
            transform.rotation = Quaternion.LookRotation(_rigidbody.velocity);
            _animator.SetBool("RUN", true);
        }
        else _animator.SetBool("RUN", false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {
            _hp--;
        }
    }
    private void Die()
    {
        if (_hp == 0)
        {
            _animator.SetBool("DIE", true);
            _uiAnimator.SetActive(true);
        }
    }
}

